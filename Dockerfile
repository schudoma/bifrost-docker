FROM ubuntu:22.04

LABEL maintainer="cschu1981@gmail.com"
LABEL version="0.1"
LABEL description="bifrost docker image"


ARG DEBIAN_FRONTEND=noninteractive

RUN apt update
RUN apt upgrade -y

RUN apt install -y wget python3-pip git dirmngr gnupg ca-certificates build-essential libssl-dev libcurl4-gnutls-dev libxml2-dev libfontconfig1-dev libharfbuzz-dev libfribidi-dev libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev cmake zlib1g-dev
RUN apt clean

RUN mkdir -p /opt/software
WORKDIR /opt/software 

RUN git clone https://github.com/pmelsted/bifrost.git
WORKDIR /opt/software/bifrost

RUN mkdir build
WORKDIR /opt/software/bifrost/build

RUN cmake .. && make && make install
